# NeuralNetworksCpp
A conversion of the neuralnetworksanddeeplearning book's python examples over to c++.
Instead of a heavy dependance on numpy I used armadillo for matrix operations.
If you are unfamiliar with armadillo just google it and they have their documentation
on their website. I think it's preety comprehensive and encompasies preety much anything
you could think of doing with a matrix. It also makes use of python for basic visualization
with matplotlib (for now lol) so you have to link with that as well.

Im gonna look up the linking and include instructions but I don't have time for that now
I just want to get this thing uploaded and ready.
